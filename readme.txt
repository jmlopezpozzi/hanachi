                           ·~: Hanachi :~·
                       Danmaku Pattern Tester
------------------------------------------------------------------------


  Hanachi is a program that lets the user play with danmaku patterns in
real time. It takes inspiration from the Pico8 "Bullet Pattern Tester"
program by rustybailey (https://www.lexaloffle.com/bbs/?pid=54400).

  The program has one emitter, the parameters of which can be set up via
command line arguments and changed during runtime using the in-screen
menu or the keyboard controls.

  The graphics used for the particles are loaded from a "sprite set".
The user can change the settings of the "sprite set" or provide custom
sets.



·Program information·
---------------------

  Hanachi is programmed in C99 using Raylib version 3.0.

  It is developed and tested on a Debian based GNU/Linux system running
on an x64 machine, using the GCC compiler version 7.5.0, having Raylib
compiled as a dynamic library, with PLATFORM_DESKTOP flag, the
OpenGL 3.3 backend, and linking GLFW externally. The GLFW version used
is 3.3.2 also compiled as a shared library.

  Hanachi is built using GNU Make. Type
      make
to build the program and
      ./hanachi
to run.

  Information on how to build Raylib can be found at its repository
(https://github.com/raysan5/raylib).

  For parsing JSON files the parson library
(https://github.com/kgabis/parson) is included in the source code (files
parson.c/.h).



·In-screen menu·
----------------

  This section introduces the parameters that can be changed from
within the in-screen menu. The menu is controlled using the arrow keys.
Up and Down choose the menu item, Left decreases the value for the
selected item and Right increases it.

LOOP ON:
      Periodicity of each pulse.
PULSE:
      Alternating sequence of emission.
ANGLE:
      Direction to which the emitter points (and emits) to.
ANGLE SPEED:
      Rotation speed used when changing the ANGLE value.
AUTO ANGLE:
      Mode in which angle is changed automatically.
BULLET SPEED:
      Speed given to the particles when emitted.
APERTURE:
      Angular range of emission, the center of which is ANGLE.
CLUSTER COUNT:
      Amount of particles created on each emission, evenly directed
      across the range defined by APERTURE and ANGLE.
SPRITE TYPE:
      Graphic set for the particles on emission.
SPRITE VARIATION:
      Variation of the SPRITE TYPE.
AUTO VARIATION:
      Mode that changes the SPRITE VARIATION on each emission.



·Keyboard controls·
-------------------

  Parameter values can also be changed using different sets of keys
bound to each parameter, which is faster than using the menu, and makes
it possible to change more than one parameter at once. Some parameters
that are not in the menu can be changed using these controls.

  Arrow keys  -> Navigate the in-screen menu
  Z           -> Hide the in-screen menu (disables its navigation)

  W, A, S, D  -> Move the emitter
  3, 2        -> Increase and decrease the speed at which the emitter moves
  1           -> Reset the emitter movement speed to its initial value

  R, F  -> Increase and decrease the LOOP ON value at each key press
  +E    -> Hold E to alter the value while the corresponding key is held
  4     -> Reset LOOP ON to its initial value

  T, G  -> Increase and decrease the PULSE value at each key press
  +E    -> Hold E to alter the value while the corresponding key is held
  5     -> Reset PULSE to its inital value

  Y, H  -> Increase and decrease ANGLE SPEED while the key is held
  6     -> Reset ANGLE SPEED to its initial value

  U, J  -> Increase and decrease the ANGLE value while the key is held
  7     -> Reset ANGLE to its initial value

  I, K  -> Increase and decrease the BULLET SPEED value while the key is held
  8     -> Reset BULLET SPEED to its initial value

  O, L  -> Increase and decrease the APERTURE value at each key press
  9     -> Reset AERTURE to its initial value

  M, N  -> Increase and decrease the CLUSTER COUNT value at each key press
  +E    -> Hold E to alter the value while the corresponding key is held
, (comma)  -> Reset CLUSTER COUNT to its inital value

  B, V  -> Change the SPRITE VARIATION
  C, X  -> Change the SPRITE TYPE

  0     -> Toggle the AUTO ANGLE option
  P     -> Toggle the AUTO VARIATION option

  F9    -> Set parameters to their inital values

  Angle controls:
    The keys on the numerical keypad are used to set the ANGLE or
  APERTURE values to predefined values. On their own they control ANGLE,
  if KP5 (keypad 5) is held, they control APERTURE.

  KP0  -> Angle 0
  KP3  -> 1/8 Circle
  KP2  -> 1/4 Circle
  KP1  -> 3/8 Circle
  KP4  -> 1/2 Circle
  KP7  -> 5/8 Circle
  KP8  -> 3/4 Circle
  KP9  -> 7/8 Circle
  KP6  -> Full Circle
  KP5  -> Held makes the keypad control APERTURE

  Screen controls:

  F1  -> Set framerate to 1FPS
  F2  -> Set framerate to 5FPS
  F3  -> Set framerate to a predefined value
  F4  -> Unlock framerate (VSYNC must be disabled for complete unlocking)
  F5  -> Toggle the display of the FPS monitor



·Command-line interface·
------------------------

  Command-line arguments can be given to the application to set
parameters at start up. Some parameters can only be set this way.

  Command-line arguments are given by writing the parameter string (two
hypens plus the parameter name) followed by a value of its corresponding
type.

  Bool types accept 0 (false) or 1 (true). The description for the
parameters of type bool explain what happens when the value 1 is passed,
if the value passed is 0, the opposite will happen.

  For parameters the values of which are of type integer or fractional,
the description includes (with some exceptions) the lowest number that
will be successfully parsed. The highest one is not given as it is
(usually) system dependent. These ranges define the values that will be
successfully read, but each parameter may impose a different valid
range. The read values are either clamped or wrapped depending on the
parameter.

--sprites          (string)
      Choose the sprite set.

--builtin-sprites  (bool)
      Force using the application generated sprites.

--pool-size        (integer >= 1)
      How many bullets (particles) the application can use. Higher
      numbers enable more complex patterns and effects at the expense of
      performance.

--width            (integer >= 1)
      Desired screen resolution width.

--height           (integer >= 1)
      Desired screen resolution height.

--scale            (integer >= 0)
      Desired screen scaling. Final screen resolution is defined by
      the width and height each multiplied by the scale. Setting it to
      0 makes the application choose the maximum possible scale value
      that will fit its screen area inside the monitor without changing
      the aspect ratio.

--vsync            (bool)
      Enable vsync.

--fps              (integer >= 1)
      Set the predefined framerate.

--unlocked-fps     (bool)
      Start the application with the framerate unlocked.

--loop-on          (integer >= 1)
      Set the inital LOOP ON value.

--pulse            (integer >= 0)
      Set the initial PULSE value.

--angle            (fractional)
      Set the initial ANGLE value.

--angular-speed    (fractional)
      Set the inital ANGLE SPEED value.

--bullet-speed     (fractional >= 0.0)
      Set the initial BULLET SPEED value.

--aperture         (fractional >= 0.0)
      Set the initial APERTURE value.

--cluster-count    (integer >= 0)
      Set the initial CLUSTER COUNT value.

--movement-speed   (fractional >= 0.0)
      Set the initial emitter MOVEMENT SPEED value.

--auto-angle       (bool)
      Start with the AUTO ANGLE option.

--auto-variation   (bool)
      Start with the AUTO VARIATION option.

--menu-disabled    (bool)
      Start with the in-screen menu disabled.

--lifetime         (integer >= 0)
      Define how many frames the application will run. 0 means it will
      not have a limit.



·Sprite sets·
-------------

  The graphics for the particles (sprites) are loaded from a sprite set.
Sprite sets are stored in the folder called "sprites". Sprite sets
consist of a folder named after the set, inside of which are a PNG image
file, a JSON sprite descriptor file and, optionally, a sprite variations
descriptor file. These files must be named like the set and ending with
".png", ".json" and ".var.json" respectively.

  The PNG file contains the graphics. The JSON file describe where the
graphics are in the PNG file. Its structure is as follows:

  An array of objects each describing one sprite. The properties of this
object are:
    "x" (Number): x coordinate of the rightmost top pixel of the sprite.
    "y" (Number): y coordinate of the rightmost top pixel of the sprite.
    "w" (Number): sprite width in pixels.
    "h" (Number): sprite height in pixels.
    "a" (Boolean)(Optional): If true the graphic rotates to match its
          angle value.

  The sprite variations descriptor file is also a JSON file. It must
contain only an object with a property of type Number named
"variations". The number must be divisible by the number of sprites
defined in the sprites descriptor file (otherwise the sprite variations
feature is ignored). Sprites are grouped consecutively in batches of the
quantity defined by the variations value, each batch being the "sprite
type" and the sprites in each batch being the variations inside the
type. For example, if a sprites descriptor file defines six sprites and
the variations descriptor file says there are two variatios per type,
sprites 1 and 2 will belong to one type, 3 and 4 to another and 5 and 6
to the last one. The variations file is optional.
