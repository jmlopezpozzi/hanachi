rlcopt = -Wall -Wextra -std=c99 -pedantic -D_DEFAULT_SOURCE -DPLATFORM_DESKTOP
rllink = -I/usr/local/include -L/usr/local/lib -lraylib -lm -lglfw
srcd = src/
bind = bin/

#NOTE: Link against GLFW using -lglfw (in rllink) if raylib was compiled without its built-in GLFW


hanachi: $(bind)main.o $(bind)hanachi.o $(bind)emitter.o $(bind)bullets.o $(bind)controller.o $(bind)control.o $(bind)sprites.o $(bind)parson.o
	gcc -o hanachi $(bind)main.o $(bind)hanachi.o $(bind)emitter.o $(bind)bullets.o $(bind)controller.o $(bind)control.o $(bind)sprites.o $(bind)parson.o $(rllink)

$(bind)main.o: $(srcd)main.c $(srcd)hanachi.h
	gcc -c $(rlcopt) -o $(bind)main.o $(srcd)main.c

$(bind)hanachi.o: $(srcd)hanachi.c $(srcd)hanachi.h $(srcd)controller.h $(srcd)emitter.h $(srcd)bullets.h $(srcd)sprites.h $(srcd)parson.h
	gcc -c $(rlcopt) -o $(bind)hanachi.o $(srcd)hanachi.c

$(bind)emitter.o: $(srcd)emitter.c $(srcd)emitter.h $(srcd)bullets.h
	gcc -c $(rlcopt) -o $(bind)emitter.o $(srcd)emitter.c

$(bind)bullets.o: $(srcd)bullets.c $(srcd)bullets.h $(srcd)sprites.h
	gcc -c $(rlcopt) -o $(bind)bullets.o $(srcd)bullets.c

$(bind)controller.o: $(srcd)controller.c $(srcd)controller.h $(srcd)control.h $(srcd)emitter.h
	gcc -c $(rlcopt) -o $(bind)controller.o $(srcd)controller.c

$(bind)control.o: $(srcd)control.c $(srcd)control.h
	gcc -c $(rlcopt) -o $(bind)control.o $(srcd)control.c

$(bind)sprites.o: $(srcd)sprites.c $(srcd)sprites.h $(srcd)parson.h
	gcc -c $(rlcopt) -o $(bind)sprites.o $(srcd)sprites.c

$(bind)parson.o: $(srcd)parson.c $(srcd)parson.h
	gcc -c $(rlcopt) -o $(bind)parson.o $(srcd)parson.c

clean:
	rm -v bin/*.o hanachi
