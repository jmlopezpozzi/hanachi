/* ctrl_wrap_i and ctrl_wrap_f test */

#include <stdio.h>
#include "../src/control.h"


#define CWF(val,min,max) \
printf("ctrl_wrap_f( val: %8s,  min: %6s,  max: %6s) = % f\n", #val, #min, #max, ctrl_wrap_f((val), (min), (max)))

#define CWI(val,min,max) \
printf("ctrl_wrap_i( val: %8s,  min: %6s,  max: %6s) = % d\n", #val, #min, #max, ctrl_wrap_i((val), (min), (max)))


int main(void)
{
	printf("Testing ctrl_wrap_i function\n");
	printf("In range:\n");

	CWI( 1,  0,  2);
	CWI( 0,  0,  2);
	CWI( 2,  0,  2);

	CWI(-1, -2,  0);
	CWI(-2, -2,  0);
	CWI( 0, -2,  0);

	CWI( 0, -2,  2);
	CWI( 1, -2,  2);
	CWI(-1, -2,  2);
	CWI(-2, -2,  2);
	CWI( 2, -2,  2);

	CWI( 2,  1,  3);
	CWI( 1,  1,  3);
	CWI( 3,  1,  3);

	CWI(-2, -3, -1);
	CWI(-3, -3, -1);
	CWI(-1, -3, -1);

	printf("Out of range:\n");

	CWI( 3,  0,  2);
	CWI(-1,  0,  2);
	CWI( 2603,  0,  2);
	CWI(-2601,  0,  2);

	CWI( 1, -2,  0);
	CWI(-3, -2,  0);
	CWI( 2601, -2,  0);
	CWI(-2603, -2,  0);

	CWI(-2, -1,  1);
	CWI( 2, -1,  1);
	CWI( 2602, -1,  1);
	CWI(-2602, -1,  1);

	CWI( 4,  1,  3);
	CWI( 0,  1,  3);
	CWI(-2,  1,  3);
	CWI( 2604,  1,  3);
	CWI(-2602,  1,  3);

	CWI(-4, -3, -1);
	CWI( 0, -3, -1);
	CWI( 2, -3, -1);
	CWI(-2604, -3, -1);
	CWI( 2602, -3, -1);


	printf("Testing ctrl_wrap_f function\n");
	printf("In range:\n");

	CWF( 0.5f,  0.0f,  1.0f);
	CWF( 0.0f,  0.0f,  1.0f);
	CWF( 1.0f,  0.0f,  1.0f);

	CWF(-0.5f, -1.0f,  0.0f);
	CWF(-1.0f, -1.0f,  0.0f);
	CWF( 0.0f, -1.0f,  0.0f);

	CWF( 0.0f, -1.0f,  1.0f);
	CWF( 0.5f, -1.0f,  1.0f);
	CWF(-0.5f, -1.0f,  1.0f);
	CWF(-1.0f, -1.0f,  1.0f);
	CWF( 1.0f, -1.0f,  1.0f);

	CWF( 1.5f,  1.0f,  2.0f);
	CWF( 1.0f,  1.0f,  2.0f);
	CWF( 2.0f,  1.0f,  2.0f);

	CWF(-1.5f, -2.0f, -1.0f);
	CWF(-2.0f, -2.0f, -1.0f);
	CWF(-1.0f, -2.0f, -1.0f);

	printf("Out of range:\n");

	CWF( 1.5f,  0.0f,  1.0f);
	CWF(-0.5f,  0.0f,  1.0f);
	CWF( 2600.5f,  0.0f,  1.0f);
	CWF(-2600.5f,  0.0f,  1.0f);

	CWF( 0.5f, -1.0f,  0.0f);
	CWF(-1.5f, -1.0f,  0.0f);
	CWF( 2600.5f, -1.0f,  0.0f);
	CWF(-2600.5f, -1.0f,  0.0f);

	CWF(-2.0f, -1.0f,  1.0f);
	CWF( 2.0f, -1.0f,  1.0f);
	CWF( 2602.0f, -1.0f,  1.0f);
	CWF(-2602.0f, -1.0f,  1.0f);

	CWF( 2.5f,  1.0f,  2.0f);
	CWF( 0.5f,  1.0f,  2.0f);
	CWF(-0.5f,  1.0f,  2.0f);
	CWF( 2600.5f,  1.0f,  2.0f);
	CWF(-2600.5f,  1.0f,  2.0f);

	CWF(-2.5f, -2.0f, -1.0f);
	CWF(-0.5f, -2.0f, -1.0f);
	CWF( 0.5f, -2.0f, -1.0f);
	CWF( 2600.5f, -2.0f, -1.0f);
	CWF(-2600.5f, -2.0f, -1.0f);

	CWF( 2.0f,  0.0f,  1.0f);
	CWF(-1.0f,  0.0f,  1.0f);

	CWF( 1.0f, -1.0f,  0.0f);
	CWF(-2.0f, -1.0f,  0.0f);

	CWF( 3.0f, -1.0f,  1.0f);
	CWF(-3.0f, -1.0f,  1.0f);

	return 0;
}
