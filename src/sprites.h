#ifndef SPRITES_H
#define SPRITES_H

#include <stdbool.h>


typedef struct Sprite_info {
	Rectangle rect;
	bool use_angle;
} Sprite_info;

typedef struct Spritemap {
	Texture2D atlas;
	size_t num_sprites;
	Sprite_info *sprites;
} Spritemap;


int spritemap_fill_data(Spritemap *map, Texture2D atlas, const char *json);
int spritemap_clear_data(Spritemap *map);


#endif
