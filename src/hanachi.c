#include <raylib.h>
#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "parson.h"
#include "sprites.h"
#include "bullets.h"
#include "emitter.h"
#include "controller.h"
#include "hanachi.h"


typedef struct Screen_info {
	int width;
	int height;
	int scale;
	int monitor_width;
	int monitor_height;
	int initial_fps;
	bool show_fps;
} Screen_info;


static void init_screen(Screen_info *screen_info,
                        const Init_values *init_values);
static int init_emitter(Emitter **emitter_ptr);
static int init_controller(Controller_obj **controller_ptr, Emitter *emitter,
                           const Init_values *init_values,
                           const Screen_info *screen_info);
static int init_pool(Bullet_pool **bullet_pool_ptr, size_t pool_size,
                     const Spritemap *spritemap,
                     const Screen_info *screen_info);
static int init_sprites(Spritemap *spritemap, const Init_values *init_values,
                        Controller_obj *controller);
static char *get_sprite_filepath(const char *base_path, const char *set,
                                 const char *ext);
static int read_to_char_array(char *dst, const char *path, size_t max_size);
static int parse_sprite_variations(const char *json);
static int generate_sprites(Spritemap *spritemap, Controller_obj *controller);
static void shutdown_sprites(Spritemap *spritemap);
static void screen_control(Screen_info *screen_info);
static void screen_showfps_render(int x, int y);


int hanachi(const Init_values *init_values)
{
	int ret = EXIT_SUCCESS;
	long frame = 0L;
	const long lifetime = init_values->lifetime;
	Screen_info screen_info;
	RenderTexture2D fb_tex;
	Spritemap spritemap;
	Bullet_pool *bullet_pool;
	Emitter *emitter;
	Controller_obj *controller;

	init_screen(&screen_info, init_values);
	fb_tex = LoadRenderTexture(screen_info.width, screen_info.height);
	if (init_emitter(&emitter)) {
		ret = EXIT_FAILURE;
		goto end_after_emitter;
	}
	if (init_controller(&controller, emitter, init_values, &screen_info)) {
		ret = EXIT_FAILURE;
		goto end_after_controller;
	}
	if (init_sprites(&spritemap, init_values, controller)) {
		ret = EXIT_FAILURE;
		goto end_after_sprites;
	}
	if (init_pool(&bullet_pool, init_values->pool_size, &spritemap,
	              &screen_info))
	{
		ret = EXIT_FAILURE;
		goto end_after_pool;
	}

	while (!WindowShouldClose()) {
		screen_control(&screen_info);
		controller_control(controller);
		emitter_beat(emitter, bullet_pool);
		bullets_update(bullet_pool);

		BeginTextureMode(fb_tex);
			ClearBackground(BLACK);
			bullets_render(bullet_pool, spritemap);
			controller_menu_render(0, 0, controller);
			#ifdef EMITTER_RENDER_INFO
			  emitter_render_info(0, 250, emitter);
			#endif
			if (screen_info.show_fps) {
				screen_showfps_render(0, screen_info.height);
			}
		EndTextureMode();
		BeginDrawing();
			DrawTexturePro(fb_tex.texture,
			               (Rectangle){.x = 0.0f, .y = 0.0f,
			                           .width = screen_info.width,
			                           .height = -screen_info.height},
			               (Rectangle){.x = 0.0f, .y = 0.0f,
			                           .width = screen_info.width *
			                                    screen_info.scale,
			                           .height = screen_info.height *
			                                     screen_info.scale},
			               (Vector2){0.0f, 0.0f}, 0.0f, WHITE);
		EndDrawing();

		frame += 1L;
		if (lifetime != 0L && frame >= lifetime) {
			break;
		}
	}

	bullet_pool_destroy(bullet_pool);
	end_after_pool:
	shutdown_sprites(&spritemap);
	end_after_sprites:
	controller_destroy(controller);
	end_after_controller:
	emitter_destroy(emitter);
	end_after_emitter:
	UnloadRenderTexture(fb_tex);
	CloseWindow();

	return ret;
}


static void init_screen(Screen_info *screen_info,
                        const Init_values *init_values)
{
	#define MIN_SCREEN_SIZE 64

	Screen_info scr;

	InitWindow(1, 1, "hanachi");
	scr.monitor_width = GetMonitorWidth(0);
	scr.monitor_height = GetMonitorHeight(0);

	scr.width = init_values->width;
	scr.height = init_values->height;
	if (scr.width > scr.monitor_width || scr.height > scr.monitor_height) {
		TraceLog(LOG_WARNING, "APP: Requested screen size (%d x %d) higher "
		                      "than monitor (%d x %d), setting default",
		                      scr.width, scr.height,
		                      scr.monitor_width, scr.monitor_height);
		scr.width = DEFAULT_SCREEN_WIDTH;
		scr.height = DEFAULT_SCREEN_HEIGHT;
	}
	else {
		if (scr.width < MIN_SCREEN_SIZE) {
			scr.width = MIN_SCREEN_SIZE;
		}
		if (scr.height < MIN_SCREEN_SIZE) {
			scr.height = MIN_SCREEN_SIZE;
		}
	}

	scr.scale = init_values->scale;
	if (scr.scale == 0 ||
	    scr.scale * scr.width > scr.monitor_width ||
	    scr.scale * scr.height > scr.monitor_height)
	{
		int scale = fmin(
			floor(scr.monitor_width / scr.width),
			floor(scr.monitor_height / scr.height)
		);
		scr.scale = scale < 1 ? 1 : scale;
	}

	scr.initial_fps = init_values->fps;
	scr.show_fps = !init_values->menu_disabled;

	CloseWindow();
	bool vsync = init_values->vsync != 0;
	SetConfigFlags(FLAG_VSYNC_HINT * vsync);
	InitWindow(scr.width * scr.scale, scr.height * scr.scale, "hanachi");
	int fps = init_values->unlocked_fps ? 0 : scr.initial_fps;
	SetTargetFPS(fps);

	*screen_info = scr;
}


static int init_emitter(Emitter **emitter_ptr)
{
	Emitter *em = emitter_create();
	if (em == NULL) {
		return 1;
	}
	*emitter_ptr = em;
	return 0;
}


static int init_controller(Controller_obj **controller_ptr, Emitter *emitter,
                           const Init_values *init_values,
                           const Screen_info *screen_info)
{
	Controller_cfg config = {
		.pos_limit_high = {.x = screen_info->width, .y = screen_info->height},
		.pos_x = screen_info->width / 2.0f,
		.pos_y = screen_info->height / 2.0f,
		.loop_on = init_values->loop_on,
		.pulse = init_values->pulse,
		.angle = init_values->angle,
		.angle_speed = init_values->angle_speed,
		.bullet_speed = init_values->bullet_speed,
		.aperture = init_values->aperture,
		.cluster_count = init_values->cluster_count,
		.movement_speed = init_values->movement_speed,
		.auto_angle = init_values->auto_angle,
		.auto_variation = init_values->auto_variation,
		.menu_disabled = init_values->menu_disabled,
	};
	Controller_obj *controller = controller_create(emitter, config);
	if (controller == NULL) {
		return 1;
	}
	*controller_ptr = controller;
	return 0;
}


static int init_pool(Bullet_pool **bullet_pool_ptr, size_t pool_size,
                     const Spritemap *spritemap, const Screen_info *screen_info)
{
	// Find largest sprite side
	const Sprite_info *sprites = spritemap->sprites;
	const size_t num_sprites = spritemap->num_sprites;
	float max = 0.0f;
	for (size_t i = 0; i < num_sprites; i += 1) {
		if (sprites[i].rect.width > max) {
			max = sprites[i].rect.width;
		}
		if (sprites[i].rect.height > max) {
			max = sprites[i].rect.height;
		}
	}
	max /= 2.0f;
	// Create bullet pool with max margin
	Vector2 limit_low = {.x = 0.0f - max, .y = 0.0f - max};
	Vector2 limit_high = {.x = screen_info->width + max,
	                      .y = screen_info->height + max};
	Bullet_pool *pool = bullet_pool_create(pool_size, limit_low, limit_high);
	if (pool == NULL) {
		return 1;
	}
	*bullet_pool_ptr = pool;
	return 0;
}


static int init_sprites(Spritemap *spritemap, const Init_values *init_values,
                        Controller_obj *controller)
{
	if (init_values->builtin_sprites) {
		goto builtin_sprites;
	}

	#define SPRITES_PATH "sprites/"
	#define JSON_MAX_SIZE 4096

	const char *set = init_values->spriteset;
	char json[JSON_MAX_SIZE] = {'\0'};

	char *descriptor_filepath = get_sprite_filepath(SPRITES_PATH, set, ".json");
	if (descriptor_filepath == NULL) {
		goto builtin_sprites;
	}
	if (read_to_char_array(json, descriptor_filepath, JSON_MAX_SIZE)) {
		free(descriptor_filepath);
		goto builtin_sprites;
	}
	free(descriptor_filepath);

	char *atlas_filepath = get_sprite_filepath(SPRITES_PATH, set, ".png");
	if (atlas_filepath == NULL) {
		goto builtin_sprites;
	}
	Texture2D sprite_atlas = LoadTexture(atlas_filepath);
	free(atlas_filepath);
	if (sprite_atlas.format == 0) {
		goto builtin_sprites;
	}

	if (spritemap_fill_data(spritemap, sprite_atlas, json)) {
		UnloadTexture(sprite_atlas);
		goto builtin_sprites;
	}
	controller_set_sprite_info(controller, spritemap->num_sprites, 1);

	// Variations are optional
	char *variations_filepath = get_sprite_filepath(SPRITES_PATH, set,
	                                                ".var.json");
	if (variations_filepath == NULL) {
		return 0;
	}
	if (read_to_char_array(json, variations_filepath, JSON_MAX_SIZE)) {
		free(variations_filepath);
		return 0;
	}
	free(variations_filepath);
	int num_variations = parse_sprite_variations(json);
	controller_set_sprite_info(controller, spritemap->num_sprites,
	                           num_variations);

	return 0;

	builtin_sprites:
	return generate_sprites(spritemap, controller);
}


static char *get_sprite_filepath(const char *base_path, const char *set,
                                 const char *ext)
{
	size_t len = strlen(base_path) + strlen(set) + strlen("/") + strlen(set) +
	             strlen(ext);
	char *filepath = calloc(len + 1, sizeof *filepath);
	if (filepath == NULL) {
		return NULL;
	}
	strcat(filepath, base_path);
	strcat(filepath, set);
	strcat(filepath, "/");
	strcat(filepath, set);
	strcat(filepath, ext);
	return filepath;
}


static int read_to_char_array(char *dst, const char *path, size_t max_size)
{
	size_t read_cursor = 0;
	FILE *fp = fopen(path, "r");
	if (fp == NULL) {
		TraceLog(LOG_WARNING, "APP: [%s] Failed to open file", path);
		return 1;
	}
	read_cursor = fread(dst, 1, max_size - 1, fp);
	dst[read_cursor] = '\0';
	TraceLog(LOG_INFO, "APP: [%s] %zu bytes read", path, read_cursor);
	fclose(fp);
	return 0;
}


static int parse_sprite_variations(const char *json)
{
	#define VARIATIONS_PROPERTY_NAME "variations"
	#define MAX_VARIATIONS 4095.0
	#define SPRITE_VARIATIONS_DEFAULT 1  // This must be 1 because sprite variations are optional

	JSON_Value *js_root = json_parse_string(json);
	if (json_value_get_type(js_root) != JSONObject) {
		goto fail;
	}
	JSON_Object *obj = json_value_get_object(js_root);
	if (!json_object_has_value(obj, VARIATIONS_PROPERTY_NAME)) {
		goto fail;
	}
	JSON_Value *val = json_object_get_value(obj, VARIATIONS_PROPERTY_NAME);
	if (json_value_get_type(val) != JSONNumber) {
		goto fail;
	}
	double variations = json_value_get_number(val);
	if (variations < 1.0 || variations > MAX_VARIATIONS) {
		goto fail;
	}
	json_value_free(js_root);

	return (int) variations;

	fail:
	TraceLog(LOG_WARNING, "APP: Could not parse sprite variations file");
	json_value_free(js_root);
	return SPRITE_VARIATIONS_DEFAULT;
}


static int generate_sprites(Spritemap *spritemap, Controller_obj *controller)
{
	#define SPRITE_SIZE 8.0f

	Image sprite = GenImageColor(SPRITE_SIZE, SPRITE_SIZE, (Color){0, 0, 0, 0});
	ImageDrawCircleV(&sprite, (Vector2){SPRITE_SIZE / 2.0f,
	                                    SPRITE_SIZE / 2.0f},
	                 (SPRITE_SIZE / 2) - 1, RED);
	ImageDrawCircleV(&sprite, (Vector2){SPRITE_SIZE / 2.0f,
	                                    SPRITE_SIZE / 2.0f},
	                 (SPRITE_SIZE / 2) - 2, RED);
	Texture2D sprite_atlas = LoadTextureFromImage(sprite);
	UnloadImage(sprite);

	const char *json = "[{\"x\":0,\"y\":0,\"w\":8,\"h\":8}]";
	if (spritemap_fill_data(spritemap, sprite_atlas, json)) {
		UnloadTexture(sprite_atlas);
		TraceLog(LOG_WARNING, "APP: Could not generate built in sprites");
		return 1;
	}
	controller_set_sprite_info(controller, spritemap->num_sprites, 1);
	TraceLog(LOG_INFO, "APP: Using built in sprites");

	return 0;

	#undef SPRITE_SIZE
}


static void shutdown_sprites(Spritemap *spritemap)
{
	UnloadTexture(spritemap->atlas);
	spritemap_clear_data(spritemap);
}


static void screen_control(Screen_info *screen_info)
{
	if (IsKeyPressed(KEY_F1)) {
		SetTargetFPS(1);
	}
	if (IsKeyPressed(KEY_F2)) {
		SetTargetFPS(5);
	}
	if (IsKeyPressed(KEY_F3)) {
		SetTargetFPS(screen_info->initial_fps);
	}
	if (IsKeyPressed(KEY_F4)) {
		SetTargetFPS(0);
	}
	if (IsKeyPressed(KEY_F5)) {
		screen_info->show_fps = !screen_info->show_fps;
	}
}


static void screen_showfps_render(int x, int y)
{
	#define FONT_SIZE 10

	DrawText(TextFormat("%6d", GetFPS()), x, y - FONT_SIZE, FONT_SIZE, GREEN);
	DrawText("FPS", x, y - FONT_SIZE * 2, FONT_SIZE, GREEN);
}
