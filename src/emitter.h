#ifndef EMITTER_H
#define EMITTER_H

#include "bullets.h"


typedef struct Emitter_public {
	float x;
	float y;
	float angle;
	float bullet_speed;
	float aperture;
	int cluster_count;
	int sprite;
} Emitter_public;

typedef struct Emitter Emitter;


Emitter *emitter_create(void);
void *emitter_destroy(Emitter *em);
Emitter_public *emitter_get_public(Emitter *em);
void emitter_set_loop_on(Emitter *em, float loop_on);
float emitter_get_loop_on(const Emitter *em);
void emitter_set_pulse(Emitter *em, int pulse);
int emitter_get_pulse(const Emitter *em);
void emitter_beat(Emitter *em, Bullet_pool *bullet_pool);
bool emitter_emitted(const Emitter *em);

#ifdef RENDER_EMITTER_INFO
  void render_emitter_info(int x, int y, const Emitter *em);
#endif


#endif
