#include <raylib.h>
#include <stdbool.h>
#include <stdlib.h>
#include <assert.h>
#include "parson.h"
#include "sprites.h"


int spritemap_fill_data(Spritemap *map, Texture2D atlas, const char *json)
{
	assert(map != NULL);
	assert(json != NULL);

	JSON_Value *js_root = json_parse_string(json);
	if (json_value_get_type(js_root) != JSONArray) {
		TraceLog(LOG_WARNING, "SPRITES: Could not parse JSON data");
		goto error_1;
	}

	JSON_Array *js_sprites = json_value_get_array(js_root);
	map->num_sprites = json_array_get_count(js_sprites);
	if (map->num_sprites == 0) {
		TraceLog(LOG_WARNING, "SPRITES: JSON data defines 0 sprites");
		goto error_1;
	}

	Sprite_info *sprites = calloc(map->num_sprites, sizeof *sprites);
	if (sprites == NULL) {
		TraceLog(LOG_WARNING, "SPRITES: Could not create Sprite_info array");
		goto error_1;
	}

	for (size_t i = 0; i < map->num_sprites; i += 1) {
		JSON_Object *obj = json_array_get_object(js_sprites, i);
		if (obj == NULL) {
			TraceLog(LOG_WARNING, "SPRITES: Could not read data for sprite %zu",
			                      i);
			goto error_2;
		}

		// Check required properties
		static const char *required[] = {"x", "y", "w", "h"};
		for (size_t j = 0; j < sizeof required / sizeof required[0]; j += 1) {
			if (!json_object_has_value(obj, required[j])) {
				TraceLog(LOG_WARNING, "SPRITES: Sprite %zu lacks required "
				                      "property \"%s\"", i, required[j]);
				goto error_2;
			}
			// All of the required values happen to be numbers, check if they are
			if (json_value_get_type(json_object_get_value(obj, required[j]))
			    != JSONNumber)
			{
				TraceLog(LOG_WARNING, "SPRITES: Property \"%s\" of sprite %zu "
				                      "is not of type Number", required[j], i);
				goto error_2;
			}
		}

		sprites[i].rect.x = json_object_get_number(obj, "x");
		sprites[i].rect.y = json_object_get_number(obj, "y");
		sprites[i].rect.width = json_object_get_number(obj, "w");
		sprites[i].rect.height = json_object_get_number(obj, "h");
		sprites[i].use_angle = false;
		if (json_object_has_value(obj, "a")) {
			JSON_Value *v = json_object_get_value(obj, "a");
			if (json_value_get_type(v) == JSONBoolean) {
				sprites[i].use_angle = json_value_get_boolean(v);
			}
		}
	}

	json_value_free(js_root);
	map->sprites = sprites;
	map->atlas = atlas;

	return 0;

	error_2:
	free(sprites);
	error_1:
	json_value_free(js_root);
	*map = (Spritemap){0};

	return 1;
}


int spritemap_clear_data(Spritemap *map)
{
	assert(map != NULL);

	free(map->sprites);
	map->sprites = NULL;
	map->num_sprites = 0;
	map->atlas = (Texture2D){0};

	return 0;
}
