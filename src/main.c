#include <limits.h>
#include <float.h>
#include <stdbool.h>
#include <stddef.h>
#include <string.h>
#include <stdlib.h>
#include <raylib.h>
#include "hanachi.h"


#define DEFAULT_SPRITESET "default"
#define DEFAULT_BUILTIN_SPRITES false
#define DEFAULT_POOL_SIZE 4096
#define DEFAULT_WIDTH (DEFAULT_SCREEN_WIDTH)  // Defined in hanachi.h
#define DEFAULT_HEIGHT (DEFAULT_SCREEN_HEIGHT)  // Defined in hanachi.h
#define DEFAULT_SCALE 0
#define DEFAULT_VSYNC true
#define DEFAULT_FPS 60
#define DEFAULT_UNLOCKED_FPS false
#define DEFAULT_LOOP_ON 10
#define DEFAULT_PULSE 0
#define DEFAULT_ANGLE (0.5 * (PI))
#define DEFAULT_ANGLE_SPEED 0.0245436926062
#define DEFAULT_BULLET_SPEED 1.0
#define DEFAULT_APERTURE 0.0
#define DEFAULT_CLUSTER_COUNT 0
#define DEFAULT_MOVEMENT_SPEED 1.0
#define DEFAULT_AUTOANGLE false
#define DEFAULT_AUTOVARIATION false
#define DEFAULT_MENU_DISABLED false
#define DEFAULT_LIFETIME 0

#define MIN_ANGLE -16777215.0
#define MAX_ANGLE 16777215.0
#define MIN_ANGLE_SPEED -65536.0
#define MAX_ANGLE_SPEED 65536.0

#define SET_SPRITESET \
(1)
#define SET_BUILTIN_SPRITES \
(1 << 1)
#define SET_POOL_SIZE \
(1 << 2)
#define SET_WIDTH \
(1 << 3)
#define SET_HEIGHT \
(1 << 4)
#define SET_SCALE \
(1 << 5)
#define SET_FPS \
(1 << 6)
#define SET_UNLOCKED_FPS \
(1 << 7)
#define SET_VSYNC \
(1 << 8)
#define SET_LOOP_ON \
(1 << 9)
#define SET_PULSE \
(1 << 10)
#define SET_ANGLE \
(1 << 11)
#define SET_ANGLE_SPEED \
(1 << 12)
#define SET_BULLET_SPEED \
(1 << 13)
#define SET_APERTURE \
(1 << 14)
#define SET_CLUSTER_COUNT \
(1 << 15)
#define SET_MOVEMENT_SPEED \
(1 << 16)
#define SET_AUTOANGLE \
(1 << 17)
#define SET_AUTOVARIATION \
(1 << 18)
#define SET_MENU_DISABLED \
(1 << 19)
#define SET_LIFETIME \
(1 << 20)
#define SET_ALL \
((1 << 21) - 1)


typedef struct Shell_parameter {
	const char *param_string;
	unsigned long vset_bitmask;
	ptrdiff_t record_offset;
	enum {T_STR, T_LNG, T_DBL, T_BOL} type;
	union {
		char c;
		struct {
			long min;
			long max;
		} l;
		struct {
			double min;
			double max;
		} d;
	} range;
} Shell_parameter;


static void get_string_arg(Init_values *init_values, const char *argv,
                           Shell_parameter param);
static void get_long_arg(Init_values *init_values, const char *argv,
                         Shell_parameter param);
static void get_double_arg(Init_values *init_values, const char *argv,
                           Shell_parameter param);
static void get_boolean_arg(Init_values *init_values, const char *argv,
                            Shell_parameter param);


int main(int argc, char *argv[])
{
	static const struct Shell_parameter params[] = {
		{
			.param_string = "--sprites",
			.vset_bitmask = SET_SPRITESET,
			.type = T_STR, .record_offset = offsetof(Init_values, spriteset),
			.range.c = '\0',
		},
		{
			.param_string = "--builtin-sprites",
			.vset_bitmask = SET_BUILTIN_SPRITES,
			.type = T_BOL, .record_offset = offsetof(Init_values, builtin_sprites),
			.range.l.min = false, .range.l.max = true,
		},
		{
			.param_string = "--pool-size",
			.vset_bitmask = SET_POOL_SIZE,
			.type = T_LNG, .record_offset = offsetof(Init_values, pool_size),
			.range.l.min = 1L, .range.l.max = INT_MAX,
		},
		{
			.param_string = "--width",
			.vset_bitmask = SET_WIDTH,
			.type = T_LNG, .record_offset = offsetof(Init_values, width),
			.range.l.min = 1L, .range.l.max = INT_MAX,
		},
		{
			.param_string = "--height",
			.vset_bitmask = SET_HEIGHT,
			.type = T_LNG, .record_offset = offsetof(Init_values, height),
			.range.l.min = 1L, .range.l.max = INT_MAX,
		},
		{
			.param_string = "--scale",
			.vset_bitmask = SET_SCALE,
			.type = T_LNG, .record_offset = offsetof(Init_values, scale),
			.range.l.min = 0L, .range.l.max = INT_MAX,
		},
		{
			.param_string = "--vsync",
			.vset_bitmask = SET_VSYNC,
			.type = T_BOL, .record_offset = offsetof(Init_values, vsync),
			.range.l.min = false, .range.l.max = true,
		},
		{
			.param_string = "--fps",
			.vset_bitmask = SET_FPS,
			.type = T_LNG, .record_offset = offsetof(Init_values, fps),
			.range.l.min = 1L, .range.l.max = INT_MAX,
		},
		{
			.param_string = "--unlocked-fps",
			.vset_bitmask = SET_UNLOCKED_FPS,
			.type = T_BOL, .record_offset = offsetof(Init_values, unlocked_fps),
			.range.l.min = false, .range.l.max = true,
		},
		{
			.param_string = "--loop-on",
			.vset_bitmask = SET_LOOP_ON,
			.type = T_LNG, .record_offset = offsetof(Init_values, loop_on),
			.range.l.min = 0L, .range.l.max = INT_MAX,
		},
		{
			.param_string = "--pulse",
			.vset_bitmask = SET_PULSE,
			.type = T_LNG, .record_offset = offsetof(Init_values, pulse),
			.range.l.min = 0L, .range.l.max = INT_MAX,
		},
		{
			.param_string = "--angle",
			.vset_bitmask = SET_ANGLE,
			.type = T_DBL, .record_offset = offsetof(Init_values, angle),
			.range.d.min = MIN_ANGLE, .range.d.max = MAX_ANGLE,
		},
		{
			.param_string = "--angular-speed",
			.vset_bitmask = SET_ANGLE_SPEED,
			.type = T_DBL, .record_offset = offsetof(Init_values, angle_speed),
			.range.d.min = MIN_ANGLE_SPEED, .range.d.max = MAX_ANGLE_SPEED,
		},
		{
			.param_string = "--bullet-speed",
			.vset_bitmask = SET_BULLET_SPEED,
			.type = T_DBL, .record_offset = offsetof(Init_values, bullet_speed),
			.range.d.min = 0.0, .range.d.max = FLT_MAX,
		},
		{
			.param_string = "--aperture",
			.vset_bitmask = SET_APERTURE,
			.type = T_DBL, .record_offset = offsetof(Init_values, aperture),
			.range.d.min = 0.0, .range.d.max = FLT_MAX,
		},
		{
			.param_string = "--cluster-count",
			.vset_bitmask = SET_CLUSTER_COUNT,
			.type = T_LNG, .record_offset = offsetof(Init_values, cluster_count),
			.range.l.min = 0L, .range.l.max = INT_MAX,
		},
		{
			.param_string = "--movement-speed",
			.vset_bitmask = SET_MOVEMENT_SPEED,
			.type = T_DBL, .record_offset = offsetof(Init_values, movement_speed),
			.range.d.min = 0.0, .range.d.max = FLT_MAX,
		},
		{
			.param_string = "--auto-angle",
			.vset_bitmask = SET_AUTOANGLE,
			.type = T_BOL, .record_offset = offsetof(Init_values, auto_angle),
			.range.l.min = false, .range.l.max = true,
		},
		{
			.param_string = "--auto-variation",
			.vset_bitmask = SET_AUTOVARIATION,
			.type = T_BOL, .record_offset = offsetof(Init_values, auto_variation),
			.range.l.min = false, .range.l.max = true,
		},
		{
			.param_string = "--menu-disabled",
			.vset_bitmask = SET_MENU_DISABLED,
			.type = T_BOL, .record_offset = offsetof(Init_values, menu_disabled),
			.range.l.min = false, .range.l.max = true,
		},
		{
			.param_string = "--lifetime",
			.vset_bitmask = SET_LIFETIME,
			.type = T_LNG, .record_offset = offsetof(Init_values, lifetime),
			.range.l.min = 0L, .range.l.max = LONG_MAX,
		},
	};

	Init_values init_values = {
		.spriteset = DEFAULT_SPRITESET,
		.pool_size = DEFAULT_POOL_SIZE,
		.width = DEFAULT_WIDTH,
		.height = DEFAULT_HEIGHT,
		.scale = DEFAULT_SCALE,
		.vsync = DEFAULT_VSYNC,
		.fps = DEFAULT_FPS,
		.unlocked_fps = DEFAULT_UNLOCKED_FPS,
		.loop_on = DEFAULT_LOOP_ON,
		.pulse = DEFAULT_PULSE,
		.angle = DEFAULT_ANGLE,
		.angle_speed = DEFAULT_ANGLE_SPEED,
		.bullet_speed = DEFAULT_BULLET_SPEED,
		.aperture = DEFAULT_APERTURE,
		.cluster_count = DEFAULT_CLUSTER_COUNT,
		.movement_speed = DEFAULT_MOVEMENT_SPEED,
		.auto_angle = DEFAULT_AUTOANGLE,
		.auto_variation = DEFAULT_AUTOVARIATION,
		.menu_disabled = DEFAULT_MENU_DISABLED,
		.lifetime = DEFAULT_LIFETIME,
	};
	unsigned long values_set = 0x0;

	for (int i = 1; i < argc; i += 1) {
		for (size_t j = 0; j < sizeof params / sizeof params[0]; j += 1) {
			const char *pstring = params[j].param_string;
			if ((values_set & params[j].vset_bitmask) != 0 ||
			    strncmp(pstring, argv[i], strlen(pstring)) != 0)
			{
				continue;
			}
			i += 1;
			if (argv[i] == NULL) {
				break;
			}
			switch (params[j].type) {
			case T_STR:
				get_string_arg(&init_values, argv[i], params[j]);
				break;
			case T_LNG:
				get_long_arg(&init_values, argv[i], params[j]);
				break;
			case T_DBL:
				get_double_arg(&init_values, argv[i], params[j]);
				break;
			case T_BOL:
				get_boolean_arg(&init_values, argv[i], params[j]);
				break;
			default:
				break;
			}
			values_set |= params[j].vset_bitmask;
		}
		if (values_set >= (SET_ALL)) {
			break;
		}
	}

	return hanachi(&init_values);
}


#define LOG_DID_NOT_PARSE(param) \
TraceLog(LOG_WARNING, "INIT: [%s] Could not parse value", (param))

#define LOG_NOT_FULLY_READ(param) \
TraceLog(LOG_WARNING, "INIT: [%s] Could not fully read argument", (param))


static void get_string_arg(Init_values *init_values, const char *argv,
                           Shell_parameter param)
{
	*(const char **)((char *)init_values + param.record_offset) = argv;
}


static void get_long_arg(Init_values *init_values, const char *argv,
                         Shell_parameter param)
{
	char *endptr = NULL;
	long val = strtol(argv, &endptr, 0);
	if (endptr == argv || val < param.range.l.min || val > param.range.l.max) {
		LOG_DID_NOT_PARSE(param.param_string);
		return;
	}
	*(long *)((char *)init_values + param.record_offset) = val;
	if (*endptr != '\0') {
		LOG_NOT_FULLY_READ(param.param_string);
	}
}


static void get_double_arg(Init_values *init_values, const char *argv,
                           Shell_parameter param)
{
	char *endptr = NULL;
	double val = strtod(argv, &endptr);
	if (endptr == argv || val < param.range.d.min || val > param.range.d.max) {
		LOG_DID_NOT_PARSE(param.param_string);
		return;
	}
	*(double *)((char *)init_values + param.record_offset) = val;
	if (*endptr != '\0') {
		LOG_NOT_FULLY_READ(param.param_string);
	}
}


static void get_boolean_arg(Init_values *init_values, const char *argv,
                            Shell_parameter param)
{
	char *endptr = NULL;
	long val = strtol(argv, &endptr, 0);
	if (endptr == argv || val < false || val > true) {
		LOG_DID_NOT_PARSE(param.param_string);
		return;
	}
	*(bool *)((char *)init_values + param.record_offset) = val;
}
