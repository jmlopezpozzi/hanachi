#ifndef CONTROL_H
#define CONTROL_H


float ctrl_press_f(float val, float delta, int key_less, int key_more);
float ctrl_hold_f(float val, float delta, int key_less, int key_more);
int ctrl_press_i(int val, int delta, int key_less, int key_more);
int ctrl_hold_i(int val, int delta, int key_less, int key_more);

float ctrl_clamp_f(float val, float min, float max);
float ctrl_wrap_f(float val, float min, float max);
int ctrl_clamp_i(int val, int min, int max);
int ctrl_wrap_i(int val, int min, int max);


#endif
