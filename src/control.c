#include <math.h>
#include <raylib.h>
#include "control.h"


float ctrl_press_f(float val, float delta, int key_less, int key_more)
{
	if (IsKeyPressed(key_less)) {
		val -= delta;
	}
	if (IsKeyPressed(key_more)) {
		val += delta;
	}
	return val;
}


float ctrl_hold_f(float val, float delta, int key_less, int key_more)
{
	if (IsKeyDown(key_less)) {
		val -= delta;
	}
	if (IsKeyDown(key_more)) {
		val += delta;
	}
	return val;
}


int ctrl_press_i(int val, int delta, int key_less, int key_more)
{
	if (IsKeyPressed(key_less)) {
		val -= delta;
	}
	if (IsKeyPressed(key_more)) {
		val += delta;
	}
	return val;
}


int ctrl_hold_i(int val, int delta, int key_less, int key_more)
{
	if (IsKeyDown(key_less)) {
		val -= delta;
	}
	if (IsKeyDown(key_more)) {
		val += delta;
	}
	return val;
}


float ctrl_clamp_f(float val, float min, float max)
{
	if (val < min) {
		return min;
	}
	if (val > max) {
		return max;
	}
	return val;
}


float ctrl_wrap_f(float val, float min, float max)
{
	if (val < min) {
		float range = max - min;
		float t = ceilf((min - val) / range);
		val = val + range * t;
	}
	else
	if (val > max) {
		float range = max - min;
		float t = ceilf((val - max) / range);
		val = val - range * t;
	}
	return val == max ? min : val;
}


int ctrl_clamp_i(int val, int min, int max)
{
	if (val < min) {
		return min;
	}
	if (val > max) {
		return max;
	}
	return val;
}


int ctrl_wrap_i(int val, int min, int max)
{
	if (val < min) {
		float range = max - min;
		int t = (min - val) / range + 1;
		val = val + range * t;
	}
	else
	if (val > max) {
		float range = max - min;
		int t = (val - max) / range + 1;
		val = val - range * t;
	}
	return val == max ? min : val;
}
