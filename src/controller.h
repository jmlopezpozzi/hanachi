#ifndef CONTROL_MENU_H
#define CONTROL_MENU_H

#include "emitter.h"


typedef struct Controller_cfg {
	Vector2 pos_limit_low;
	Vector2 pos_limit_high;
	float pos_x;
	float pos_y;
	int loop_on;
	int pulse;
	float angle;
	float angle_speed;
	float bullet_speed;
	float aperture;
	int cluster_count;
	float movement_speed;
	bool auto_angle;
	bool auto_variation;
	bool menu_disabled;
} Controller_cfg;

typedef struct Controller_obj Controller_obj;


Controller_obj *controller_create(Emitter *emitter, Controller_cfg config);
void *controller_destroy(Controller_obj *ctl);
void controller_set_sprite_info(Controller_obj *ctl, int num_sprites,
                                int num_variations);
void controller_control(Controller_obj *ctl);
void controller_menu_render(int x, int y, const Controller_obj *ctl);


#endif
