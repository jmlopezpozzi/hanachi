#include <stdlib.h>
#include <stdbool.h>
#include <stddef.h>
#include <assert.h>
#include "bullets.h"
#include "emitter.h"


struct Emitter {
	Emitter_public public;
	float loop_on;
	float frame;
	int pulse;
	int pulse_count;
	bool emitted;
};


static void emit(Emitter_public emp, Bullet_pool *pool);


Emitter *emitter_create(void)
{
	Emitter *emitter = calloc(1, sizeof *emitter);
	if (emitter == NULL) {
		TraceLog(LOG_WARNING, "EMITTER: Could not create Emitter");
		return NULL;
	}
	emitter->loop_on = 1.0f;

	return emitter;
}


void *emitter_destroy(Emitter *em)
{
	free(em);
	return NULL;
}


Emitter_public *emitter_get_public(Emitter *em)
{
	return &(em->public);
}


void emitter_set_loop_on(Emitter *em, float loop_on)
{
	assert(loop_on >= 1.0f);
	em->loop_on = loop_on;
	if (em->frame > em->loop_on) {
		em->frame = em->loop_on;
	}
}


float emitter_get_loop_on(const Emitter *em)
{
	return em->loop_on;
}


void emitter_set_pulse(Emitter *em, int pulse)
{
	assert(pulse >= 0);
	em->pulse = pulse;
	em->pulse_count = pulse;
}


int emitter_get_pulse(const Emitter *em)
{
	return em->pulse;
}


void emitter_beat(Emitter *em, Bullet_pool *bullet_pool)
{
	em->emitted = false;
	if (em->frame >= em->loop_on) {
		em->frame -= em->loop_on;
		em->pulse_count += 1;
		if (em->pulse_count > em->pulse) {
			emit(em->public, bullet_pool);
			em->emitted = true;
		}
		if (em->pulse_count >= (em->pulse << 1)) {
			em->pulse_count = 0;
		}
	}
	em->frame += 1.0f;
}


static void emit(Emitter_public emp, Bullet_pool *pool)
{
	float current_angle = emp.angle - emp.aperture / 2.0f;
	float step_angle = emp.aperture / emp.cluster_count;

	for (int i = 0; i <= emp.cluster_count; i += 1) {
		bullet_shoot(pool, emp.x, emp.y, current_angle, emp.bullet_speed,
		             emp.sprite);
		current_angle += step_angle;
	}
}


bool emitter_emitted(const Emitter *em)
{
	return em->emitted;
}


#ifdef EMITTER_RENDER_INFO
void emitter_render_info(int x, int y, Emitter *em)
{
	#define LINE_HEIGHT 15
	#define FONT_SIZE 10

	struct Menu {
		const char *fmt;
		enum {
			T_INT,
			T_FLT,
		} typ;
		ptrdiff_t val;
	};

	static const struct Menu menu[] = {
		{.fmt = "FRAME = %f",       .typ = T_FLT, .val = offsetof(Emitter, frame)},
		{.fmt = "PULSE_COUNT = %d", .typ = T_INT, .val = offsetof(Emitter, pulse_count)},
	};

	for (size_t i = 0; i < sizeof menu / sizeof menu[0]; i += 1) {
		const char *cur_text = menu[i].typ == T_FLT ?
			TextFormat(menu[i].fmt, *(float *)((char *)em + menu[i].val)) :
			TextFormat(menu[i].fmt, *(int *)((char *)em + menu[i].val));

		DrawText(cur_text, x, y + LINE_HEIGHT * i, FONT_SIZE, WHITE);
	}
}
#endif
