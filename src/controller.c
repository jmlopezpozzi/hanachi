#include <stddef.h>
#include <stdlib.h>
#include "emitter.h"
#include "control.h"
#include "controller.h"


#define LOOP_ON_MIN 1
#define LOOP_ON_MAX 4095
#define LOOP_ON_MIN_F 1.0f
#define LOOP_ON_MAX_F 4095.0f
#define PULSE_MIN 0
#define PULSE_MAX 4095
#define ANGLE_SPEED_DELTA 0.0024543692606f
#define APERTURE_DELTA 0.0245436926062f
#define BULLET_SPEED_DELTA 0.025f
#define BULLET_SPEED_MIN 0.25f
#define BULLET_SPEED_MAX 4095.0f
#define MOVEMENT_SPEED_DELTA 0.05f
#define MOVEMENT_SPEED_MIN 0.05f
#define MOVEMENT_SPEED_MAX 50.0f
#define CLUSTER_COUNT_MIN 0
#define CLUSTER_COUNT_MAX 4095


enum options {
	OP_LOOP_ON = 0,
	OP_PULSE,
	OP_ANGLE,
	OP_ANGLE_SPEED,
	OP_AUTO_ANGLE,
	OP_BULLET_SPEED,
	OP_APERTURE,
	OP_CLUSTER_COUNT,
	OP_SPRITE_TYPE,
	OP_SPRITE_VARIATION,
	OP_AUTO_VARIATION,
	OP_MOVEMENT_SPEED,
	NUM_OPTIONS,
};

struct Controller_obj {
	Emitter *emitter;
	float movement_speed;
	float angle_speed;
	int auto_angle;
	int auto_variation;
	struct {
		int type;
		int num_types;
		int variation;
		int num_variations;
	} sprite;
	Controller_cfg defaults;
	enum options current_option;
	bool menu_disabled;
};


static void control_menu(Controller_obj *ctl, Emitter_public *emp, Emitter *em);
static void control_defaults(Controller_obj *ctl, Emitter *em,
                             Emitter_public *emp);
static void control_reset_defaults(Controller_obj *ctl, Emitter *em,
                                   Emitter_public *emp);
static void control_movement_x(Emitter_public *emp, const Controller_obj *ctl,
                               int key_more, int key_less);
static void control_movement_y(Emitter_public *emp, const Controller_obj *ctl,
                               int key_more, int key_less);
static void control_loop_on(Emitter *em, int key_less, int key_more);
static void control_pulse(Emitter *em, int key_less, int key_more);
static void control_angle(Emitter_public *emp, const Controller_obj *ctl,
                          int key_less, int key_more);
static void control_angle_speed(Controller_obj *ctl, int key_less,
                                int key_more);
static void control_bullet_speed(Emitter_public *emp, int key_less,
                                 int key_more);
static void control_aperture(Emitter_public *emp, int key_less, int key_more);
static void control_cluster_count(Emitter_public *emp, int key_less,
                                  int key_more);
static void control_sprite_type(Emitter_public *emp, Controller_obj *ctl,
                                int key_less, int key_more);
static void control_sprite_variation(Emitter_public *emp, Controller_obj *ctl,
                                     int key_less, int key_more);
static void control_movement_speed(Controller_obj *ctl, int key_less,
                                   int key_more);
static void control_auto_angle(Controller_obj *ctl, int key_less, int key_more);
static void control_auto_variation(Controller_obj *ctl, int key_less,
                                   int key_more);
static void absolute_angle_selector(Emitter_public *emp);


Controller_obj *controller_create(Emitter *emitter, Controller_cfg config)
{
	Controller_obj *ctl = malloc(sizeof *ctl);
	if (ctl == NULL) {
		TraceLog(LOG_WARNING, "CONTROLLER: Could not create Controller_obj");
		return NULL;
	}

	config.loop_on = ctrl_clamp_i(config.loop_on, LOOP_ON_MIN, LOOP_ON_MAX);
	config.pulse = ctrl_clamp_i(config.pulse, PULSE_MIN, PULSE_MAX);
	config.angle = ctrl_wrap_f(config.angle, 0.0f, 2.0f * PI);
	config.bullet_speed = ctrl_clamp_f(config.bullet_speed, BULLET_SPEED_MIN,
	                                   BULLET_SPEED_MAX);
	config.aperture = ctrl_clamp_f(config.aperture, 0.0f, 2.0f * PI);
	config.cluster_count = ctrl_clamp_i(config.cluster_count, CLUSTER_COUNT_MIN,
	                                    CLUSTER_COUNT_MAX);
	config.movement_speed = ctrl_clamp_f(config.movement_speed,
	                                     MOVEMENT_SPEED_MIN,
	                                     MOVEMENT_SPEED_MAX);

	Emitter_public *emp = emitter_get_public(emitter);
	emp->x = config.pos_x;
	emp->y = config.pos_y;
	emp->angle = config.angle;
	emp->bullet_speed = config.bullet_speed;
	emp->aperture = config.aperture;
	emp->cluster_count = config.cluster_count;
	emp->sprite = 0;
	emitter_set_loop_on(emitter, config.loop_on);
	emitter_set_pulse(emitter, config.pulse);

	ctl->emitter = emitter;
	ctl->movement_speed = config.movement_speed;
	ctl->angle_speed = config.angle_speed;
	ctl->auto_angle = config.auto_angle;
	ctl->auto_variation = config.auto_variation;
	ctl->menu_disabled = config.menu_disabled;
	ctl->sprite.type = 0;
	ctl->sprite.num_types = 1;
	ctl->sprite.variation = 0;
	ctl->sprite.num_variations = 1;
	ctl->current_option = 0;
	ctl->defaults = config;

	return ctl;
}


void *controller_destroy(Controller_obj *ctl)
{
	free(ctl);
	return NULL;
}


void controller_set_sprite_info(Controller_obj *ctl, int num_sprites,
                                int num_variations)
{
	if (num_sprites < 1) {
		num_sprites = 1;
	}
	if (num_variations < 1) {
		num_variations = 1;
	}
	if (num_sprites % num_variations != 0) {
		num_variations = 1;
	}
	ctl->sprite.num_types = num_sprites / num_variations;
	ctl->sprite.num_variations = num_variations;
}


void controller_control(Controller_obj *ctl)
{
	Emitter *em = ctl->emitter;
	Emitter_public *emp = emitter_get_public(ctl->emitter);

	if (IsKeyPressed(KEY_Z)) {
		ctl->menu_disabled = !ctl->menu_disabled;
	}
	if (!ctl->menu_disabled) {
		control_menu(ctl, emp, em);
	}

	control_movement_x(emp, ctl, KEY_A, KEY_D);
	control_movement_y(emp, ctl, KEY_W, KEY_S);
	control_movement_speed(ctl, KEY_TWO, KEY_THREE);
	control_loop_on(em, KEY_F, KEY_R);
	control_pulse(em, KEY_G, KEY_T);
	control_angle_speed(ctl, KEY_H, KEY_Y);
	control_angle(emp, ctl, KEY_J, KEY_U);
	control_bullet_speed(emp, KEY_K, KEY_I);
	control_aperture(emp, KEY_L, KEY_O);
	control_cluster_count(emp, KEY_N, KEY_M);
	control_sprite_type(emp, ctl, KEY_X, KEY_C);
	control_sprite_variation(emp, ctl, KEY_V, KEY_B);
	control_auto_angle(ctl, KEY_ZERO, KEY_ZERO);
	control_auto_variation(ctl, KEY_P, KEY_P);

	control_defaults(ctl, em, emp);

	if (ctl->auto_angle == true) {
		float val = emp->angle + ctl->angle_speed;
		emp->angle = ctrl_wrap_f(val, 0.0f, 2.0f * PI);
	}
	if (ctl->auto_variation == true && emitter_emitted(em)) {
		int val = ctl->sprite.variation + 1;
		val = ctrl_wrap_i(val, 0, ctl->sprite.num_variations);
		ctl->sprite.variation = val;
		emp->sprite = ctl->sprite.type * ctl->sprite.num_variations + val;
	}

	absolute_angle_selector(emp);
}


static void control_menu(Controller_obj *ctl, Emitter_public *emp, Emitter *em)
{
	ctl->current_option = ctrl_press_i(ctl->current_option, 1, KEY_UP,
	                                   KEY_DOWN);
	ctl->current_option = ctrl_wrap_i(ctl->current_option, 0, NUM_OPTIONS);

	switch (ctl->current_option) {
	case OP_LOOP_ON:
		control_loop_on(em, KEY_LEFT, KEY_RIGHT);
		break;
	case OP_PULSE:
		control_pulse(em, KEY_LEFT, KEY_RIGHT);
		break;
	case OP_ANGLE:
		control_angle(emp, ctl, KEY_LEFT, KEY_RIGHT);
		break;
	case OP_ANGLE_SPEED:
		control_angle_speed(ctl, KEY_LEFT, KEY_RIGHT);
		break;
	case OP_AUTO_ANGLE:
		control_auto_angle(ctl, KEY_LEFT, KEY_RIGHT);
		break;
	case OP_BULLET_SPEED:
		control_bullet_speed(emp, KEY_LEFT, KEY_RIGHT);
		break;
	case OP_APERTURE:
		control_aperture(emp, KEY_LEFT, KEY_RIGHT);
		break;
	case OP_CLUSTER_COUNT:
		control_cluster_count(emp, KEY_LEFT, KEY_RIGHT);
		break;
	case OP_SPRITE_TYPE:
		control_sprite_type(emp, ctl, KEY_LEFT, KEY_RIGHT);
		break;
	case OP_SPRITE_VARIATION:
		control_sprite_variation(emp, ctl, KEY_LEFT, KEY_RIGHT);
		break;
	case OP_AUTO_VARIATION:
		control_auto_variation(ctl, KEY_LEFT, KEY_RIGHT);
		break;
	case OP_MOVEMENT_SPEED:
		control_movement_speed(ctl, KEY_LEFT, KEY_RIGHT);
		break;
	case NUM_OPTIONS:
	default:
		break;
	}
}


void controller_menu_render(int x, int y, const Controller_obj *ctl)
{
	if (ctl->menu_disabled) {
		return;
	}

	Emitter *em = ctl->emitter;
	Emitter_public *emp = emitter_get_public(ctl->emitter);

	#define LINE_HEIGHT 10
	#define FONT_SIZE 10

	struct Menu {
		const char *fmt;
		enum {
			T_INT,
			T_FLT,
			T_INT_CALL,
			T_FLT_CALL,
			O_INT,
			O_FLT,
		} typ;
		ptrdiff_t val;
	};

	static const struct Menu menu[] = {
		{.fmt = "LOOP ON = %.0f",     .typ = T_FLT_CALL, .val = (ptrdiff_t) emitter_get_loop_on},
		{.fmt = "PULSE = %d",         .typ = T_INT_CALL, .val = (ptrdiff_t) emitter_get_pulse},
		{.fmt = "ANGLE = %f",         .typ = T_FLT, .val = offsetof(Emitter_public, angle)},
		{.fmt = "ANGLE SPEED = %f",   .typ = O_FLT, .val = offsetof(Controller_obj, angle_speed)},
		{.fmt = "AUTO ANGLE = %d",    .typ = O_INT, .val = offsetof(Controller_obj, auto_angle)},
		{.fmt = "BULLET SPEED = %f",  .typ = T_FLT, .val = offsetof(Emitter_public, bullet_speed)},
		{.fmt = "APERTURE = %f",      .typ = T_FLT, .val = offsetof(Emitter_public, aperture)},
		{.fmt = "CLUSTER COUNT = %d", .typ = T_INT, .val = offsetof(Emitter_public, cluster_count)},
		{.fmt = "SPRITE TYPE = %d",   .typ = O_INT, .val = offsetof(Controller_obj, sprite.type)},
		{.fmt = "SPRITE VARIATION = %d", .typ = O_INT, .val = offsetof(Controller_obj, sprite.variation)},
		{.fmt = "AUTO VARIATION = %d",   .typ = O_INT, .val = offsetof(Controller_obj, auto_variation)},
		{.fmt = "MOVEMENT SPEED = %.2f", .typ = O_FLT, .val = offsetof(Controller_obj, movement_speed)},
	};

	for (size_t i = 0; i < sizeof menu / sizeof menu[0]; i += 1) {
		const char *cur_text;

		switch (menu[i].typ) {
		case T_INT:
			cur_text = TextFormat(menu[i].fmt,
			                      *(int *)((char *)emp + menu[i].val));
			break;
		case T_FLT:
			cur_text = TextFormat(menu[i].fmt,
			                      *(float *)((char *)emp + menu[i].val));
			break;
		case T_INT_CALL:
			cur_text = TextFormat(menu[i].fmt,
			                      ((int (*)(Emitter *))(menu[i].val))(em));
			break;
		case T_FLT_CALL:
			cur_text = TextFormat(menu[i].fmt,
			                      ((float (*)(Emitter *))(menu[i].val))(em));
			break;
		case O_INT:
			cur_text = TextFormat(menu[i].fmt,
			                      *(int *)((char *)ctl + menu[i].val));
			break;
		case O_FLT:
			cur_text = TextFormat(menu[i].fmt,
			                      *(float *)((char *)ctl + menu[i].val));
			break;
		default:
			cur_text = "Unrecognized type";
			break;
		}

		DrawText(cur_text, x, y + LINE_HEIGHT * i, FONT_SIZE,
		         i == ctl->current_option ? YELLOW : WHITE);
	}
}


static void control_defaults(Controller_obj *ctl, Emitter *em,
                             Emitter_public *emp)
{
	if (IsKeyPressed(KEY_F9)) {
		control_reset_defaults(ctl, em, emp);
		return;
	}
	if (IsKeyPressed(KEY_ONE)) {
		ctl->movement_speed = ctl->defaults.movement_speed;
	}
	if (IsKeyPressed(KEY_FOUR)) {
		emitter_set_loop_on(em, ctl->defaults.loop_on);
	}
	if (IsKeyPressed(KEY_FIVE)) {
		emitter_set_pulse(em, ctl->defaults.pulse);
	}
	if (IsKeyPressed(KEY_SIX)) {
		ctl->angle_speed = ctl->defaults.angle_speed;
	}
	if (IsKeyPressed(KEY_SEVEN)) {
		emp->angle = ctl->defaults.angle;
	}
	if (IsKeyPressed(KEY_EIGHT)) {
		emp->bullet_speed = ctl->defaults.bullet_speed;
	}
	if (IsKeyPressed(KEY_NINE)) {
		emp->aperture = ctl->defaults.aperture;
	}
	if (IsKeyPressed(KEY_COMMA)) {
		emp->cluster_count = ctl->defaults.cluster_count;
	}
}


static void control_reset_defaults(Controller_obj *ctl, Emitter *em,
                                   Emitter_public *emp)
{
	Controller_cfg defaults = ctl->defaults;

	emp->x = defaults.pos_x;
	emp->y = defaults.pos_y;
	emp->angle = defaults.angle;
	emp->bullet_speed = defaults.bullet_speed;
	emp->aperture = defaults.aperture;
	emp->cluster_count = defaults.cluster_count;
	emitter_set_loop_on(em, defaults.loop_on);
	emitter_set_pulse(em, defaults.pulse);

	ctl->movement_speed = defaults.movement_speed;
	ctl->angle_speed = defaults.angle_speed;
	ctl->auto_angle = defaults.auto_angle;
	ctl->auto_variation = defaults.auto_variation;
	ctl->menu_disabled = defaults.menu_disabled;
}


static void control_movement_x(Emitter_public *emp, const Controller_obj *ctl,
                               int key_more, int key_less)
{
	float val = ctrl_hold_f(emp->x, ctl->movement_speed, key_more, key_less);
	emp->x = ctrl_clamp_f(val, ctl->defaults.pos_limit_low.x,
	                           ctl->defaults.pos_limit_high.x);
}


static void control_movement_y(Emitter_public *emp, const Controller_obj *ctl,
                               int key_more, int key_less)
{
	float val = ctrl_hold_f(emp->y, ctl->movement_speed, key_more, key_less);
	emp->y = ctrl_clamp_f(val, ctl->defaults.pos_limit_low.y,
	                           ctl->defaults.pos_limit_high.y);
}


static void control_loop_on(Emitter *em, int key_less, int key_more)
{
	float (*ctrl_f)(float, float, int, int) = ctrl_press_f;
	if (IsKeyDown(KEY_E)) {
		ctrl_f = ctrl_hold_f;
	}
	float cur_val = emitter_get_loop_on(em);
	float new_val = ctrl_f(cur_val, 1.0f, key_less, key_more);
	if (new_val != cur_val) {
		emitter_set_loop_on(em, ctrl_clamp_f(new_val, LOOP_ON_MIN_F,
		                                     LOOP_ON_MAX_F));
	}
}


static void control_pulse(Emitter *em, int key_less, int key_more)
{
	int (*ctrl_i)(int, int, int, int) = ctrl_press_i;
	if (IsKeyDown(KEY_E)) {
		ctrl_i = ctrl_hold_i;
	}
	int cur_val = emitter_get_pulse(em);
	int new_val = ctrl_i(cur_val, 1, key_less, key_more);
	if (new_val != cur_val) {
		emitter_set_pulse(em, ctrl_clamp_i(new_val, PULSE_MIN, PULSE_MAX));
	}
}


static void control_angle(Emitter_public *emp, const Controller_obj *ctl,
                          int key_less, int key_more)
{
	float val = ctrl_hold_f(emp->angle, ctl->angle_speed, key_less, key_more);
	emp->angle = ctrl_wrap_f(val, 0.0f, 2.0f * PI);
}


static void control_angle_speed(Controller_obj *ctl, int key_less, int key_more)
{
	ctl->angle_speed = ctrl_hold_f(ctl->angle_speed, ANGLE_SPEED_DELTA,
	                               key_less, key_more);
}


static void control_bullet_speed(Emitter_public *emp, int key_less,
                                 int key_more)
{
	float val = ctrl_hold_f(emp->bullet_speed, BULLET_SPEED_DELTA, key_less,
	                        key_more);
	emp->bullet_speed = ctrl_clamp_f(val, BULLET_SPEED_MIN, BULLET_SPEED_MAX);
}


static void control_aperture(Emitter_public *emp, int key_less, int key_more)
{
	float val = ctrl_hold_f(emp->aperture, APERTURE_DELTA, key_less, key_more);
	emp->aperture = ctrl_clamp_f(val, 0.0f, 2.0f * PI);
}


static void control_cluster_count(Emitter_public *emp, int key_less,
                                  int key_more)
{
	int (*ctrl_i)(int, int, int, int) = ctrl_press_i;
	if (IsKeyDown(KEY_E)) {
		ctrl_i = ctrl_hold_i;
	}
	int val = ctrl_i(emp->cluster_count, 1, key_less, key_more);
	emp->cluster_count = ctrl_clamp_i(val, CLUSTER_COUNT_MIN,
	                                  CLUSTER_COUNT_MAX);
}


static void control_sprite_type(Emitter_public *emp, Controller_obj *ctl,
                                int key_less, int key_more)
{
	int val = ctrl_press_i(ctl->sprite.type, 1, key_less, key_more);
	val = ctrl_wrap_i(val, 0, ctl->sprite.num_types);
	ctl->sprite.type = val;
	emp->sprite = val * ctl->sprite.num_variations + ctl->sprite.variation;
}


static void control_sprite_variation(Emitter_public *emp, Controller_obj *ctl,
                                     int key_less, int key_more)
{
	int val = ctrl_press_i(ctl->sprite.variation, 1, key_less, key_more);
	val = ctrl_wrap_i(val, 0, ctl->sprite.num_variations);
	ctl->sprite.variation = val;
	emp->sprite = ctl->sprite.type * ctl->sprite.num_variations + val;
}


static void control_movement_speed(Controller_obj *ctl, int key_less,
                                   int key_more)
{
	float val = ctrl_hold_f(ctl->movement_speed, MOVEMENT_SPEED_DELTA, key_less,
	                        key_more);
	ctl->movement_speed = ctrl_clamp_f(val, MOVEMENT_SPEED_MIN,
	                                   MOVEMENT_SPEED_MAX);
}


static void control_auto_angle(Controller_obj *ctl, int key_less, int key_more)
{
	if (IsKeyPressed(key_less) || IsKeyPressed(key_more)) {
		ctl->auto_angle = !ctl->auto_angle;
	}
}


static void control_auto_variation(Controller_obj *ctl, int key_less,
                                   int key_more)
{
	if (IsKeyPressed(key_less) || IsKeyPressed(key_more)) {
		ctl->auto_variation = !ctl->auto_variation;
	}
}


static void absolute_angle_selector(Emitter_public *emp)
{
	float *target = IsKeyDown(KEY_KP_5) ? &(emp->aperture) : &(emp->angle);
	if (IsKeyPressed(KEY_KP_6)) {
		*target = PI * 2.0f;
	}
	if (IsKeyPressed(KEY_KP_3)) {
		*target = PI * 0.25f;
	}
	if (IsKeyPressed(KEY_KP_2)) {
		*target = PI * 0.5f;
	}
	if (IsKeyPressed(KEY_KP_1)) {
		*target = PI * 0.75f;
	}
	if (IsKeyPressed(KEY_KP_4)) {
		*target = PI;
	}
	if (IsKeyPressed(KEY_KP_7)) {
		*target = PI * 1.25f;
	}
	if (IsKeyPressed(KEY_KP_8)) {
		*target = PI * 1.5f;
	}
	if (IsKeyPressed(KEY_KP_9)) {
		*target = PI * 1.75f;
	}
	if (IsKeyPressed(KEY_KP_0)) {
		*target = 0.0f;
	}
}
