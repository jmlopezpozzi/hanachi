#include <raylib.h>
#include <stdlib.h>
#include <stdbool.h>
#include <math.h>
#include "sprites.h"
#include "bullets.h"


typedef struct Bullet {
	float x;
	float y;
	float vx;
	float vy;
	float angle;
	int sprite;
} Bullet;


typedef struct Bullet_entry {
	union {
		Bullet live;
		struct Bullet_entry *next;
	} state;
	bool in_use;
} Bullet_entry;


struct Bullet_pool {
	size_t size;
	Vector2 limit_low;
	Vector2 limit_high;
	Bullet_entry *first;
	Bullet_entry bullets[];  // Flexible array member
};


Bullet_pool *bullet_pool_create(size_t pool_size, Vector2 limit_low,
                                Vector2 limit_high)
{
	Bullet_pool *pool = malloc(sizeof *pool + pool_size * sizeof(Bullet_entry));
	if (pool == NULL) {
		TraceLog(LOG_WARNING, "BULLETS: Could not create Bullet_pool of size "
		                      "%zu", pool_size);
		return NULL;
	}
	pool->size = pool_size;
	pool->limit_low = limit_low;
	pool->limit_high = limit_high;
	pool->first = pool->bullets;

	Bullet_entry *bullets = pool->bullets;
	for (size_t i = 0; i < pool_size - 1; i += 1) {
		bullets[i].state.next = &(bullets[i + 1]);
		bullets[i].in_use = false;
	}
	bullets[pool_size - 1].state.next = NULL;
	bullets[pool_size - 1].in_use = false;

	return pool;
}


void *bullet_pool_destroy(Bullet_pool *pool)
{
	free(pool);
	return NULL;
}


void bullet_shoot(Bullet_pool *pool, float x, float y, float angle, float speed,
                  int sprite)
{
	Bullet_entry *entry = pool->first;
	if (entry == NULL) {
		return;
	}
	pool->first = entry->state.next;
	entry->in_use = true;

	Bullet *bullet = &(entry->state.live);
	bullet->x = x;
	bullet->y = y;
	bullet->vx = speed * cosf(angle);
	bullet->vy = speed * sinf(angle);
	bullet->angle = (angle * 360.0f) / (2.0f * PI);
	bullet->sprite = sprite;
}


void bullets_update(Bullet_pool *pool)
{
	size_t pool_size = pool->size;
	Vector2 limit_low = pool->limit_low;
	Vector2 limit_high = pool->limit_high;
	Bullet_entry *bullets = pool->bullets;

	for (size_t i = 0; i < pool_size; i += 1) {
		if (bullets[i].in_use == false) {
			continue;
		}

		Bullet *bullet = &(bullets[i].state.live);
		bullet->x += bullet->vx;
		bullet->y += bullet->vy;
		if (bullet->x < limit_low.x || bullet->x > limit_high.x ||
		    bullet->y < limit_low.y || bullet->y > limit_high.y)
		{
			bullets[i].in_use = false;
			bullets[i].state.next = pool->first;
			pool->first = &(bullets[i]);
		}
	}
}


void bullets_render(const Bullet_pool *pool, Spritemap spritemap)
{
	//#define CONTROL_PIXEL

	size_t pool_size = pool->size;
	const Bullet_entry *bullets = pool->bullets;
	for (size_t i = 0; i < pool_size; i += 1) {
		if (bullets[i].in_use == false) {
			continue;
		}

		const Bullet *bullet = &(bullets[i].state.live);
		const Sprite_info *spr = &(spritemap.sprites[bullet->sprite]);
		DrawTexturePro(spritemap.atlas,
		               spr->rect,
		               (Rectangle){.x = bullet->x, .y = bullet->y,
		                           .width = spr->rect.width,
		                           .height = spr->rect.height},
		               (Vector2){.x = spr->rect.width / 2.0f,
		                         .y = spr->rect.height / 2.0f},
		               spr->use_angle * bullet->angle, WHITE);

		#ifdef CONTROL_PIXEL
		DrawRectangleV((Vector2){bullet->x, bullet->y}, (Vector2){1.0f, 1.0f},
		               YELLOW);
		#endif
	}
}
