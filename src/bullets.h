#ifndef BULLETS_H
#define BULLETS_H

#include <raylib.h>
#include <stddef.h>
#include "sprites.h"


typedef struct Bullet_pool Bullet_pool;


Bullet_pool *bullet_pool_create(size_t pool_size, Vector2 limit_low,
                                Vector2 limit_high);
void *bullet_pool_destroy(Bullet_pool *pool);
void bullet_shoot(Bullet_pool *pool, float x, float y, float angle,
                  float speed, int sprite);
void bullets_update(Bullet_pool *pool);
void bullets_render(const Bullet_pool *pool, Spritemap spritemap);


#endif
