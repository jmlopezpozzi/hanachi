#ifndef HANACHI_H
#define HANACHI_H


#define DEFAULT_SCREEN_WIDTH 240
#define DEFAULT_SCREEN_HEIGHT 320


typedef struct Init_values {
	char *spriteset;
	size_t pool_size;
	long width;
	long height;
	long scale;
	long fps;
	long loop_on;
	long pulse;
	double angle;
	double angle_speed;
	double bullet_speed;
	double aperture;
	long cluster_count;
	double movement_speed;
	long lifetime;
	bool builtin_sprites;
	bool vsync;
	bool unlocked_fps;
	bool auto_angle;
	bool auto_variation;
	bool menu_disabled;
} Init_values;


int hanachi(const Init_values *init_values);


#endif
